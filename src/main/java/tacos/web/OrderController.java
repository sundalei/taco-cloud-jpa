package tacos.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.support.SessionStatus;
import tacos.Order;
import tacos.User;
import tacos.data.OrderRepository;

import java.util.List;

@Controller
@RequestMapping("/orders")
@SessionAttributes("order")
public class OrderController {

    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    private OrderRepository orderRepo;

    @Autowired
    public OrderController(OrderRepository orderRepo) {
        this.orderRepo = orderRepo;
    }

    @GetMapping("/current")
    public String orderForm(@AuthenticationPrincipal User user, @ModelAttribute Order order) {
        if(order.getDeliveryName() == null) {
            order.setDeliveryName(user.getFullname());
        }
        if(order.getDeliveryStreet() == null) {
            order.setDeliveryStreet(user.getStreet());
        }
        if(order.getDeliveryCity() == null) {
            order.setDeliveryCity(user.getCity());
        }
        if(order.getDeliveryState() == null) {
            order.setDeliveryState(user.getState());
        }
        if(order.getDeliveryZip() == null) {
            order.setDeliveryZip(user.getZip());
        }

        return "orderForm";
    }
    
    @PostMapping
    public String processOrder(@Valid Order order, Errors errors, SessionStatus sessionStatus, @AuthenticationPrincipal User user) {
    	if(errors.hasErrors()) {
    		return "orderForm";
    	}
    	
    	log.info("Order submitted: " + order);
    	order.setUser(user);

    	orderRepo.save(order);
    	sessionStatus.setComplete();

    	return "redirect:/";
    }

    @PostMapping(value = "/showOrder", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<Order> showOrder(String deliveryZip, Model model) {
        List<Order> showOrder = orderRepo.findByDeliveryZip(deliveryZip);
        return showOrder;
    }
}
