package tacos.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import tacos.Ingredient;
import tacos.data.IngredientRepository;

import java.util.Optional;

@Component
public class IngredientByIdConverter implements Converter<String, Ingredient> {
	
	private static final Logger log = LoggerFactory.getLogger(IngredientByIdConverter.class);
	
	private IngredientRepository ingredientRepository;

	@Autowired
	public IngredientByIdConverter(IngredientRepository ingredientRepository) {
		this.ingredientRepository = ingredientRepository;
	}

	@Override
	public Ingredient convert(String id) {
		log.info("IngredientByIdConverter convert id: " + id);
		return this.ingredientRepository.findById(id).orElse(null);
	}

}
